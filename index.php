<?php
session_start();

if(isset($_SESSION['userLogin'])){
    if($_SESSION['userLogin']['userRange']=="admin"){
        header('Location: admin/');

    }else if($_SESSION['userLogin']['userRange']=="usuario"){
        header('Location: cliente/');
        
    }
}

?>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <link rel="shortcut icon" href="images/pos.jpg">
        
        
        <link rel="stylesheet" type="text/css" href="styles.css">
        <script type="text/javascript" src="js/jquery.js"></script>
        

        <title>Snack</title>
    </head>
    <body>
        <header>
            <nav>
                <div>SNACK</div>
                <ul>
                    <li><a href="index.php?">Home</a></li>
                    <li><a href="">Products</a></li>
                    <li><a href="">Info</a></li>
                    <li><a href="">Register</a></li>
                    <li><a  class="active" href="">Log In</a></li>
                </ul>
            </nav>
        </header>
        <section class="sec1"></section>
        <section class="content"></section>
    </body>
</html>
