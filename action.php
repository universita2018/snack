<?php
//action.php
incude_once("conexion.php")
session_start();
if(isset($_POST["product_id"]))
{
	$order_table='';
	$message='';
	if($_POST["action"]=="add"){
		if(isset($_SESSION["carrito"])){
			$is_available=0;
			foreach ($_SESSION["carrito"] as $keys => $values) 
			{
				if($_SESSION["carrito"][$keys]['product_id']==$_POST["product_id"])
				{
					$is_available++;
					$_SESSION["carrito"][$keys]['product_quantity']=$_SESSION["carrito"][$keys]['product_quantity']+$_POST["product_quantity"];

				}
			}
			if($is_available<1)
			{
				$item_array=array(
					'product_id'=>$_POST["product_id"],
					'product_name'=>$_POST["product_name"],
					'product_prize'=>$_POST["product_prize"],
					'product_quantity'=>$_POST["product_quantity"],
				);
				$_SESSION["carrito"][]=$item_array;

			}
		}
		else{
			$item_array=array(
				'product_id'=>$_POST["product_id"],
					'product_name'=>$_POST["product_name"],
					'product_prize'=>$_POST["product_prize"],
					'product_quantity'=>$_POST["product_quantity"],
			);
			$_SESSION["carrito"][]=$item_array;
		}
	}
	if($_POST["action"]=="remove")
	{
		foreach ($_SESSION["carrito"] as $keys => $values) 
		{
			if($values["product_id"]==$_POST["product_id"])
			{
				unset($_SESSION["carrito"][$keys]);
				$message='<label class="text-success">Product Removed</label>';
			}
		}

	}
	if($_POST["action"]=="quantity_change")
	{
		foreach ($_SESSION["carrito"] as $keys => $values) 
		{
			if($_SESSION["carrito"][$keys]["product_id"]==$_POST["product_id"])
			{
				$_SESSION["carrito"][$keys]['product_quantity']=$_POST["quantity"];
			}
		}

	}

		$order_table.='
			<table class="table table-bordered"> 
				<tr>
					<th width="40%">Product Name</th>
					<th width="10%">Quantity</th>
					<th width="20%">Price</th>
					<th width="15%">total </th>
					<th></th>
					<th width="5%">Action</th>
				</tr>
		';
		if(!empty($_SESSION["carrito"])){

			total=0;
			foreach ($_SESSION["carrito"] as $keys => $values)
			{
				$order_table.='
				
				<tr>
					<td width="40%">'.$values["product_name"].'</td>
					<td width="10%"><input type="text" name="quantity[]" id="quantity'.$values["product_id"].'"  value="'.$values["product_quantity"].'" class=" form-control quantity" data-product_id="'.$values["product_id"].'" />'.$values["product_quantity"].'</td>
					<td align="right">'.$values["product_prize"].'</td>
					<td>'.number_format($values["product_quantity"]*$values["product_prize"],2).'</td>
					<td><button name="delete" class="btn btn-danger btn-xs delete" id="'.$values["product_id"].'"></button></td>
				</tr>
		';
		$total=$total+($values["product_quantity"]*$values["product_prize"]);
			}
			$order_table.='
				
				<tr>
					
					<td colspan="3" align="right">Total</td>
					<td align="right">'.number_format($total,2).'</td>
					<td></td>
				</tr>
		';
		}
		$order_table.='</table>';
		$output=array(
			$output=array(
				'order_table '=> $order_table,
				'cart_item'   => count($_SESSION["carrito"])
			);
			echo json_encode($output);
		);
	
}
?>