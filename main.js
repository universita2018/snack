jQuery(document).on('submit','#formlg',function(event){
	event.preventDefault();

	jQuery.ajax({
		url: 'login.php',
		type:'POST',
		dataType:'json',
		data:$(this).serialize(),
		beforeSend:function(){
			$('.botton').val('Validando..');
		}
	})
	.done(function(respuesta){
		console.log("respuesta");
		if(!respuesta.error){
			if(respuesta.tipo=='admin')
				{
					location.href='/admin/';
				}
		else if(respuesta.tipo=='usuario')
				{
					location.href='/cliente/';
				}
		}else{
			$('.error').sliceDown('slow');
			setTimeout(function(){
				$('.error').slideUp('slow');
			},3000);
			$('.botton').val('Iniciar Session');
		}

	})
	.fail(function(resp){
		console.log(resp.responseText);
	})
	.always(function(){
		console.log("complete");
	});
});