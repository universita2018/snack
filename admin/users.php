<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <link rel="shortcut icon" href="/images/pos.jpg">
        <link href="/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/DT_bootstrap.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/styles.css">
        <link rel="stylesheet" href="/css/bootstrap.css">
        <script type="text/javascript" src="/js/jquery.js"></script>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  

        <title>Users</title>
    </head>
    <body>
        <div class="container">
            <h1></h1>
            <button class="btn btn-primary" data-toggle="modal" data-target="#RegistrarUser">ADD User</button>
        </div>
        <table class="table">
            <thead class="active">
                <tr class="active">
                    <td>FirstName<td>
                    <td>LastName<td>
                    <td>email<td>
                    <td>address<td>
                    <td>phone<td>
                    <td>username<td>
                    <td>password<td>
                    <td>range<td>
                    <td>Edit</td>
                    <td>Delete</td>   
                </tr> 
            </thead>
           <tbody class="contenido">
               <?php 
                    include '../funtions/tableUser.php';
               ?>
           </tbody>

        </table>
        <script type="text/javascript">

        $(document).ready(function () {

            $('#RegistrarUser').click(function () {
                var firstname=$('#firstname').val();
                var lastname=$('#lastname').val();
                var email=$('#email').val();
                var address=$('#address').val();
                var phone=$('#phone').val();
                var username=$('#username').val();
                var password=$('#password').val();
                var range=$('#range').val();

                $.ajax({

                    type:'POST',
                    url:'../funtions/registrarUser.php',
                    data:'firstname'+userFirstname+'&lastname'+userLastname+'email'+userEmail+'&phone'+'&address'+userAddress+userPhone+'&username'+userLogin+'&password'+userPassword+'&range'+userRange,
                    success: function(){
                        $('.contenido').load('../funtions/tableUser.php');
                        
                    }
                });

            });
        });
    </script>

    <div id="registrarUser" class="modal fade" role="dialog">

        <div class="modal-dialog">
            
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">*</button>
                    <h4 class="modal-title">Agregar persona</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Nombre
                        </label>
                        <div class="field desc">
                            <input type="text" id="firstname" name="userFirstname" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Apellido
                        </label>
                        <div class="field desc">
                            <input type="text" id="lastname" name="userLastname" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Email
                        </label>
                        <div class="field desc">
                            <input type="text" id="email" name="userEmail" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Telefono
                        </label>
                        <div class="field desc">
                            <input type="text" id="phone" name="userPhone" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Direccion
                        </label>
                        <div class="field desc">
                            <input type="text" id="address" name="userAddress" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Login
                        </label>
                        <div class="field desc">
                            <input type="text" id="username" name="userLogin" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Password
                        </label>
                        <div class="field desc">
                            <input type="text" id="password" name="userPassword" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">tipo de usuario
                        </label>
                        <div class="field desc">
                            <input type="text" id="range" name="userRange" required="required">
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls controls-row" >
                        
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    </body>

</html>
