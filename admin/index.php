<?php
session_start();

if(isset($_SESSION['userLogin'])){
    if($_SESSION['userLogin']['userRange']=="admin"){
        header('Location: index.php');

    }else if($_SESSION['userLogin']['userRange']=="usuario"){
        header('Location: ../index.php');
        
    }
}

?>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <link rel="shortcut icon" href="/images/pos.jpg">
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/DT_bootstrap.css">
        <link rel="stylesheet" href="../css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="styles1.css">
        <script type="text/javascript" src="../js/query.js"></script>
        <script type="text/javascript">
            function toggleSidebar(){
                document.getElementById("sidebar").classList.toggle('active');

            }
        </script>
        <title>admin</title>
    </head>
    <body>
        <div id="sidebar">
            <div class="toggle-btn" onclick="toggleSidebar();">
                <span></span>
                <span></span>
            </div>
            
                <ul>
                    <li>Products</li>
                    <li>Orders</li>
                    <li>Users</li>
                    <li>Categoties</li>
                </ul>
            
        </div>
        <!--<h1><?php echo $_SESSION['userLogin']; ?></h1>
        <a href="../logout.php">Cerrar session</a>-->
    </body>
</html>
