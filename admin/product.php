<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <link rel="shortcut icon" href="/images/pos.jpg">
        <link href="/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/DT_bootstrap.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/styles.css">
        <link rel="stylesheet" href="/css/bootstrap.css">
        <script type="text/javascript" src="/js/jquery.js"></script>
        

        <title>Products</title>
    </head>
    <body>
        <?php 
            if(isset($_GET["idproduct"])){

                $idproduct=$_GET['id_product'];
                $name=$_GET['productName'];
                $description=$_GET['productDescription'];
                $type=$_GET['productType'];
                $prize=$_GET['productPrize'];
                $stock=$_GET['productStock'];
                $foto=$_GET['productFoto'];
               

                echo "<input type="hidden" id="Nid_product" value="$id_product">";
                echo "<input type="hidden" id="Nname" value="$name">";
                echo "<input type="hidden" id="Ndescription" value="$description">";
                echo "<input type="hidden" id="Ntype" value="$type">";
                echo "<input type="hidden" id="Nprize" value="$prize">";
                echo "<input type="hidden" id="Nstock" value="$stock">";
                echo "<input type="hidden" id="Nfoto" value="$foto">";
                

            }
        ?>
        <div class="container">
            <h1></h1>
            <button class="btn btn-primary" data-toggle="modal" data-target="#registrarProd">ADD Product</button>
        </div>
        <?php
            else{
        ?>
        <table class="table">
            <thead class="active">
                <tr class="active">
                    <td>idproduct<td>
                    <td>Name<td>
                    <td>description<td>
                    <td>Type<td>
                    <td>Prize<td>
                    <td>Stock<td>
                    <td>Foto<td>
                </tr> 
            </thead>
           <tbody class="contenido">
               <?php 
                    include 'tableProduct.php';
               ?>
           </tbody>

        </table>
        <?php 
            }
        ?>
        <script type="text/javascript" src="js/crudUser.js">
    </script>

    <div id="registrarProd" class="modal fade" role="dialog">
        <h1>PRODUCT REGISTER</h1>
        <div class="modal-dialog">
            
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">add</button>
                    <h4 class="modal-title">Agregar producto</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Nombre del producto:
                        </label>
                        <div class="field desc">
                            <input type="text" id="name" name="productName" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Descripcion:
                        </label>
                        <div class="field desc">
                            <input type="text" id="description" name="productDescription" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Tipo:
                        </label>
                        <div class="field desc">
                            <input type="" id="type" name="productType" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Precio:
                        </label>
                        <div class="field desc">
                            <input type="number" id="prize" name="productPrize" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Stock:
                        </label>
                        <div class="field desc">
                            <input type="number" id="stock" name="productStock" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="inputPatient">Foto:
                        </label>
                        <div class="field desc">
                            <input type="file" id="foto" name="productFoto" required="required">
                        </div>
                    </div>
                </div>
               

                <div class="control-group">
                    <div class="controls controls-row" >
                        
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <button type="submit" class="btn btn-primary" data-dismiss="modal" id="Registrar">Registrar</button>
    </div>
    </body>

</html>
