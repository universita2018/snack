<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <link rel="shortcut icon" href="/images/pos.jpg">
        <link href="/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/DT_bootstrap.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/styles.css">
        <link rel="stylesheet" href="/css/bootstrap.css">
        <script type="text/javascript" src="/js/jquery.js"></script>
        

        <title>Categories</title>
    </head>
    <body>
        <?php 
            if(isset($_GET["id"])){

                $id=$_GET['id'];
                $type=$_GET['type'];
                
                echo "<input type="hidden" id="Nid" value="$id">";
                echo "<input type="hidden" id="Ntype" value="$type">";
                
            }
        ?>
        <div class="container">
            <h1></h1>
            <button class="btn btn-primary" data-toggle="modal" data-target="#registrarType">ADD Category</button>
        </div>
        <?php
            else{
        ?>
        <table class="table">
            <thead class="active">
                <tr class="active">
                    <td>Code<td>
                    <td>Type<td>
                    <td></td>
                    <td></td>
                </tr> 
            </thead>
           <tbody class="contenido">
               <?php 
                    include '../funtions/tableType.php';
               ?>
           </tbody>

        </table>
        <?php 
            }
        ?>
        <script type="text/javascript" src="js/crudType.js">
    </script>

    <div id="registrarType" class="modal fade" role="dialog">

        <div class="modal-dialog">
            
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">*</button>
                    <h4 class="modal-title">Agregar </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="id">codigo
                        </label>
                        <div class="field desc">
                            <input type="text" id="id" name="id" readonly="readonly" >
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="type">Tipo
                        </label>
                        <div class="field desc">
                            <input type="text" id="type" name="type" required="required">
                        </div>
                    </div>
                </div>
                

                
            </div>
            
        </div>
        
    </div>
    <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <button type="submit" class="btn btn-primary" data-dismiss="modal" id="Registrar">Registrar</button>
        
    </div>
    </body>

</html>
