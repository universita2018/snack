<?php

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH'])&&strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest'){

	require '../conexion.php';

	sleep(2);

	$mysqli_set_charset('utf8');
	$user=mysqli_real_escape_string($_POST['userLogin']);
	$password=mysqli_real_escape_string($_POST['userPassword']);
	if($consult=mysqli_prepare("SELECT userLogin, userRange FROM user WHERE userLogin=? AND userPassword=?")){

		$consult->bind_param('ss',$user,$password);
		$consult->execute();
		$result=$consult->get_result();

		if($result->num_rows==1){
			$datos=$result->fetch_assoc();
			echo json_encode(array('error'=>false,'tipo'=>$datos['userRange']));
		}else{
			echo json_encode(array('error'=>true));
		}
		$consult->close();
	}

}

$mysqli->close();
?>