<?php  
 //cart.php  
session_start();  
 $connect = mysqli_connect("localhost", "root", "", "shopping_cart");  
?>  
<!DOCTYPE html>  
<html>  
      <head> 

        <link rel="shortcut icon" href="images/pos.jpg">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/DT_bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  

      </head>  
      <body>  
           <br />  
           <div class="container" style="width:800px;">  
                <?php  
                if(isset($_POST["place_order"]))  
                {  
                     $insert_order = "  
                     INSERT INTO order(idclient, orderDate, orderStatus)  
                     VALUES('', '".date('Y-m-d')."', 'P')  
                     ";  
                     $order_id = "";  
                     if(mysqli_query($connect, $insert_order))  
                     {  
                          $order_id = mysqli_insert_id($connect);  
                     }  
                     $_SESSION["order_id"] = $order_id;  
                     $order_details = "";  
                     foreach($_SESSION["carrito"] as $keys => $values)  
                     {  
                          $order_details .= "  
                          INSERT INTO detailorder(idproduct,idOrder,detailOrderPrize, detailOrderQuantity)  
                          VALUES('".$idorder."', '".$values["idproduct"]."', '".$values["detailOrderPrice"]."', '".$values["detailOrderQuantity"]."');  
                          ";  
                     }  
                     if(mysqli_multi_query($connect, $order_details))  
                     {  
                          unset($_SESSION["carrito"]);  
                          echo '<script>alert("You have successfully place an order...Thank you")</script>';  
                          echo '<script>window.location.href="cart.php"</script>';  
                     }  
                }  
                if(isset($_SESSION["order_id"]))  
                {  
                     $customer_details = '';  
                     $order_details = '';  
                     $total = 0;  
                     $query = '  
                     SELECT * FROM order  
                     INNER JOIN detailorder 
                     ON detailorder.idOrder = order.idorder  
                     INNER JOIN client  
                     ON client.idclient = order.idclient  
                     WHERE order.idorder= "'.$_SESSION["order_id"].'"  
                     ';  
                     $result = mysqli_query($connect, $query);  
                     while($row = mysqli_fetch_array($result))  
                     {  
                          $customer_details = '  
                          <label>'.$row["clientUser"].'</label> 
                          <p>'.$row["clientLastname"].'</p>
                          <p>'.$row["clientAddress"].'</p>  
                          <p>'.$row["clientLastname"].', '.$row["clientFirstname"].'</p>  
                          <p>'.$row["clientEmail"].'</p>  
                          ';  
                          $order_details .= "  
                               <tr>  
                                    <td>".$row["productName"]."</td>  
                                    <td>".$row["productQuantity"]."</td>  
                                    <td>".$row["productPrize"]."</td>  
                                    <td>".number_format($row["product_quantity"] * $row["product_price"], 2)."</td>  
                               </tr>  
                          ";  
                          $total = $total + ($row["product_quantity"] * $row["product_price"]);  
                     }  
                     echo '  
                     <h3 align="center">Order Summary for Order No.'.$_SESSION["order_id"].'</h3>  
                     <div class="table-responsive">  
                          <table class="table">  
                               <tr>  
                                    <td><label> Details</label></td>  
                               </tr>  
                               <tr>  
                                    <td>'.$customer_details.'</td>  
                               </tr>  
                               <tr>  
                                    <td><label>Order Details</label></td>  
                               </tr>  
                               <tr>  
                                    <td>  
                                         <table class="table table-bordered">  
                                              <tr>  
                                                   <th width="50%">Product Name</th>  
                                                   <th width="15%">Quantity</th>  
                                                   <th width="15%">Price</th>  
                                                   <th width="20%">Total</th>  
                                              </tr>  
                                              '.$order_details.'  
                                              <tr>  
                                                   <td colspan="3" align="right"><label>Total</label></td>  
                                                   <td>'.number_format($total, 2).'</td>  
                                              </tr>  
                                         </table>  
                                    </td>  
                               </tr>  
                          </table>  
                     </div>  
                     ';  
                }  
                ?>  
           </div>  
      </body>  
 </html>  