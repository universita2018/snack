<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <link rel="shortcut icon" href="images/pos.jpg">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/DT_bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="styles.css">
        
        <title>Snack</title>
    </head>
    <body>
        <div class="error">
            <span>Datos de Ingreso no son validos, intentalo de nuevo</span>
        </div>
        <div class="main">
            <form action="" id="formlg">
                <input type="text" name="userLogin" placeholder="User" required="required" pattern="[A-Za-z0-9_-]{1,20}">
                <input type="password" name="userPassword" placeholder="password" required="required">
                <input type="submit" class="botton" value="Iniciar Session">
            </form>
        </div>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="main.js"></script>
    </body>
</html>
