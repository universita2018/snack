<?php 
session_start();
include_once ("../conexion.php");
?>
<!DOCTYPE html>  
<html>  
 <head>  
  <title>Shopping Cart</title>  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
 </head>  
 <body>  
  <br /><br />  
  <div class="container" style="width:800px;">  
     <h3 align="center">Shopping Cart</h3>  
     <br />
     <ul class="nav nav-tabs">
       <li class="active"><a data-toggle="tab" href="#products">Products</a></li>
       <li class="active"><a data-toggle="tab" href="#cart">Cart<span class="badge"><?php if(isset($_SESSION["carrito"])){echo count($_SESSION["carrito"]);}else {echo '0';}?></span></a></li>
     </ul>
    <div class="tab-content" >
      <div id="products" class="tab-pane fade in active">
        <?php 
          $sql="SELECT * FROM product ORDER BY idproduct ASC ";
          $res=mysqli_query($con,$sql);
          while ($row=mysqli_fetch_array($res)) {
         ?>
          <div class="col-md-4" style="margin-top: 12px;">
            <div style="border:1px solid #333; background-color: #f1f1f1;">
              <img src="images/<?php echo $row["productFoto"];?>"/>
              <h4 class="text-info"><?php echo $row[productName];?></h4>
              <h4 class="text-info"><?php echo $row[productType];?></h4>
              <h4 class="text-info"><?php echo $row[productDescription];?></h4>
              <h4 class="text-danger"><?php echo $row[productPrize];?></h4>
              <input type="text" name="quantity" id="quantity<?php echo $row["product_id"]; ?>" class="form-control" value="1"/> 
              <input type="hidden" name="hidden_name" id="name<?php echo $row["product_id"]; ?>" value="<?php echo $row["productName"];?>"/> 
              <input type="hidden" name="hidden_prize" id="prize<?php echo $row["product_id"]; ?>" value="<?php echo $row["productPrize"];?>"/> 
              <input type="button" name="add_to_cart" id="name<?php echo $row["product_id"]; ?>" class="add_to_cart" value="add_to_cart"/> 
            </div>
          </div>
         <?php   
          }
        ?>
      </div>
      <div id="cart" class="tab-pane fade ">
        <div class="table-responsive" id="order_table">
          <table class="table table-bordered">
            <tr>
                <th width="40%">Product Name</th>
                <th width="10%">Quantity</th>
                <th width="20%">Price</th>
                <th width="15%">total </th>
                <th></th>
                <th width="5%">Action</th>
            </tr>
            <?php 
                if(!empty($_SESSION["carrito"]))
                {
                  $total=0;
                  foreach ($_SESSION["carrito"] as $keys => $values)
                    {
                      ?>
                      <tr>
                        <td><?php echo $values["product_name"]; ?></td>
                        <td width="10%"><input type="text" name="quantity[]" id="quantity<?php echo $values["product_id"] ;?>" value="<?php echo $values["product_quantity"];?>" data-product_id="quantity<?php echo $values["product_id"] ;?>" class="quantity" /></td>
                        <td align="right">$ <?php echo $values["product_prize"]; ?></td>
                        <td>$ <?php echo number_format($values["product_quantity"]*$values["product_prize"],2) ?></td>
                        <td><button name="delete" class="btn btn-danger btn-xs delete" id="'.$values["product_id"].'">Remove</button></td>
                      </tr>
                  
                      <?php
                      $total=$total+($values["product_quantity"]*$values["product_prize"]);
                    }
                    ?>
                    <tr>
                      <td colspan="3" align="right">Total</td>
                      <td align="right">$ <?php number_format($total,2);
                      ?></td>
                      <td></td>
                    </tr>
                    <?php
                }
            ?>
          </table>
          
        </div>
        
      </div>
    </div>
  </div>

 </body>  
</html>

<script type="text/javascript">
$(document).ready(function(data){
  $('.add_to_cart').click(function(){
    var product_id=$(this).attr("product_id");
    var product_name=$('#name'+product_id).val();
    var product_prize=$('#prize'+product_id).val();
    var product_quantity=$('#quantity'+product_id).val();
    var action="add";
    if(product_quantity>0)
    {
      $.ajax({
        url:"action.php",
        method:"POST",
        dataType:"json",
        data:{
          product_id:product_id,
          product_name:product_name,
          product_quantity:product_quantity,
          action:action
        },
        success:function(data)
        {
           $('#order_table').html(data.order_table); 
           $('.badge').text(data.cart_item);
           alert("Product has been Added into cart");
        }
      });
    }
    else{
      alert("Write number of quantity")
    }

  });

  $(document).on('click','.delete',function(){
    var product_id=$(this).attr("product_id");
    var action="remove";
    if(confirm("Are you sure you want to remove this prod?"))
    {
      $.ajax({
        url:"action.php",
        method:"POST",
        dataType:"json",
        data:{product_id:product_id,action:action},
        success:function(data){
            $('#order_table').html(data.order_table);
            $('.badge').text(data.cart_item);
        }
      });
    }
    else
    {
        return false;
    }
  });
      $(document).on('keyup','.quantity',function(){
      var product_id=$(this).data("product_id");
      var quantity=$(this).val();
      var action="quantity_change";
      if(quantity!='')
      {
        $.ajax({
          url:"action.php",
          method:"POST",
          dataType:"json",
          data:{product_id:product_id,quantity:quantity,action:action},
          success:function(data){
              $('#order_table').html(data.order_table);
              
          }
        });
      }
      
    });


});  
</script>
